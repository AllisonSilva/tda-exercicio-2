#include <stdio.h>
#include <math.h>

int main()

{
    int init, razao, n, pa,
        count;

    printf("Elemento inicial da P.A.: \n");
    scanf("%d", &init);

    printf("Razao da P.A.: \n");
    scanf("%d", &razao);

    printf("Numero de termos da P.A.: \n");
    scanf("%d", &n);

    for (count = 0; count <= n; count++)
    {
        pa = init + count * razao;
        printf("Elemento %d da P.A.: %d \n", count, pa);
    }
}

