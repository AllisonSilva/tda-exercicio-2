#include <stdio.h>
int main() {
    
    int i, n, prox, t1 = 0, t2 = 1;
    
    scanf("%d", &n);
    for(i = 0; i <= n; i++){
        if(i != n){
            printf("%d ", t1);
            prox = t1 + t2;
            t1 = t2;
            t2 = prox;
        }
        else
        {
            printf("%d\n", t1);
        }   
    }
    return 0;
}
