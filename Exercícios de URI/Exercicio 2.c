#include <stdio.h>

int main(){
    
    float horas, media, litros;

    scanf("%f", &horas);
    scanf("%f", &media);
    
    litros = (horas * media) /12;
    
    printf("%.3f\n", litros);
    
    return 0;
}