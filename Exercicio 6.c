#include <stdio.h>
#include <math.h>

int main()

{
    int init, razao, n, pg, count;

    printf("Elemento inicial da P.G.: \n");
    scanf("%d", &init);

    printf("Razao da P.G.: \n");
    scanf("%d", &razao);

    printf("Numero de termos da P.G.: \n");
    scanf("%d", &n);

    for (count = 0; count <= n; count++)
    {
        pg = init*pow(razao, count-1) + 0.000001;
        printf("Elemento %d da P.G.: %d \n", count, pg);
    }
}
